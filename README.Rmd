---
title: "Gel 2021"
author: "DRAAF Auvergne-Rhône-Alpes"
date: "`r format(Sys.Date(), '%d %B %Y')`"
output: 
  html_document: 
    toc: yes
    theme: united
    number_sections: yes
bibliography: gel_2021.bib
link-citations: true 
---

<style>
body {margin-bottom:5em}
.toc-section-number:after, .header-section-number:after {content:". "}
div#TOC li {list-style:none}
</style>

# Objectifs

Obtenir des données de températures minimales pour chaque commune de la région et estimer les surfaces potentielles des différentes cultures touchées par le gel et le nombre d'exploitations impactées.

## Interpolation de données ponctuelles températures pour créer une couverture continue

À partir de points (données stations météo MétéoFrance), modéliser la température en tout point via une régression sur l'altitude (MNT) puis krigeage, pour obtenir les valeurs de température pour toutes les communes : méthode de regression-kriging [@henglPracticalGuideGeostatistical2007].

Plusieurs modèles ont été testé avec le krigeage (GLM, RandomForest, GAM) par validation croisée à 10 blocs ; c'est le GAM qui était le meilleur.

Le MNT de la région de résolution 100 m est créé à partir de la BDALTI IGN téléchargée sur Internet (si non disponible)

<img src="resultats/temperatures_minimales_r84_20210408.jpeg" width="800"/>

## Surfaces de vergers par commune

Correction du RA 2010 pour être en cohérence avec la SAA provisoire 2020 et jointure entre données météo communales et données surfaces pour évaluer les surfaces de verger touchées par différentes classes de gel par espèce.

Utilisation d'un export Balsa d'avril 2020 pour compter les exploitations par classes de gel.

## Livrables

-   raster des températures
-   couche de points par commune avec les infos surfaciques cultures et température mini
-   couches des stations météo
-   graphique de répartition des surfaces touchées par le gel, par cultures  
    <img src="resultats/graphique_gel_vignes_vergers_r84_2021-04-08.png" width="800"/>
-   fichiers tableurs récapitulatifs surfaces et nombre d'"ateliers" par cultures/dép./classe de température ; nb d'exploitations par dép./classe de température
-   cartes de localisation des productions/températures (projets QGIS à ouvrir par ailleurs)  
    <img src="resultats/surf_pechers_temp_mini_r84_20210408.jpeg" width="800"/>

# Mode d'emploi

1.  Télécharger les données MétéoFrance (payantes, demander un compte "république numérique" pour accès gratuit) sur la publithèque <https://publitheque.meteo.fr/> : toutes les stations de niveau 0, 1, 2 de la région : températures minimales, avec coordonnées Lambert 2 étendu hectométriques.  
    Stocker dans *donnees/meteo*.

2.  Récupérer le fichier RA 2010 sur CERISE :  
    *03-Espace-de-Diffusion/030_Structures_exploitations/3020_Recensements/RA_2010/RA2010_CASD_donnees/fic_sav/*. Le fichier *2_RA2010_cultures.sav* est à mettre dans *donnees/ra_2010/2_RA2010_cultures.sav*

3.  Balsa 2020 (pour le nombre d'exploitations) : *CERISE/03-Espace-de-Diffusion/000_Referentiels/0020_Balsa_EA/BALSA_29_AVRIL_NB_EA_FRUITS*. Le fichier *BALSA_29_AVRIL_FRUITS.rds* est à mettre dans *donnees/ra_2020/BALSA_29_AVRIL_FRUITS.rds*.

4.  Modifier *donnees/saa_fruits_2020.xlsx*.

5.  Modifier dans *regression_kriging_vergers.R* les valeurs de :

    -   dep
    -   reg
    -   reg_ra
    -   region
    -   date_donnees : date du fichier météo
    -   fichier_meteo : emplacement du fichier météo

6.  Remplacer le logo si besoin : *donnees/logo_prefet_aura_2020.png*.

7.  Décommenter la configuration du proxy et mettre vos login/password Agricoll si vous êtes sur le RIE.

8.  Exécuter *regression_kriging_vergers.R*.  
    NB : 7z doit être installé (dans *C:\\Program Files\\7-Zip\\7z.exe*) pour la décompression des fichiers MNT

9.  En bonus : le script *tmin_synop.R* permet de créer le graphique des températures minimales du dernier mois pour mettre en évidence les périodes de gel à partir des données SYNOP (gratuites) de Météofrance.  
    <img src="resultats/tmin_mois_r84_2021-04-08.png" width="800"/>

# Références
